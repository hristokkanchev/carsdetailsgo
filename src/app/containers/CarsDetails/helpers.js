import axios from 'axios';

import { API_GET_CARS } from './constants';
import {
  carsDataFetching,
  carsDataFetched,
  carsDataFetchFail,
} from './actions';

/**
 * Requests new cars from API.
 * @param {Function} dispatch Redux dispatch function
 */
export function requestAddCars(dispatch) {
  dispatch(carsDataFetching());

  axios.get(API_GET_CARS)
  .then(function (response) {
    dispatch(carsDataFetched(response.data));
  })
  .catch(function (error) {
    dispatch(carsDataFetchFail(error.message));
  });
}