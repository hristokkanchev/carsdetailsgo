import {
  CARS_DATA_FETCHING,
  CARS_DATA_FETCHED,
  CARS_DATA_FETCH_FAIL
} from './constants';

/**
 * Starts fetching cars data.
 */
export function carsDataFetching() {
  return {
    type: CARS_DATA_FETCHING,
    payload: {
      loading: true
    }
  };
}

/**
 * Dispatches action to redux store to update cars.
 * @param {Object} carsInfo 
 */
export function carsDataFetched(carsInfo) {
  return {
    type: CARS_DATA_FETCHED,
    payload: {
      loading: false,
      carsInfo,
    }
  };
}

/**
 * Dispatches action to print errors.
 * @param {String} errorMessage Handlers errors.
 */
export function carsDataFetchFail(errorMessage) {
  return {
    type: CARS_DATA_FETCH_FAIL,
    payload: {
      loading: false,
      errorMessage
    }
  };
}
