// Constants.

export const CARS_DATA_FETCHING = 'CARS_DATA_FETCHING';

export const CARS_DATA_FETCHED = 'CARS_DATA_FETCHED';

export const CARS_DATA_FETCH_FAIL = 'CARS_DATA_FETCH_FAIL';

export const API_GET_CARS = '/api/cars';
