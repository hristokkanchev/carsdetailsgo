import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { requestAddCars } from './helpers';

import AddCarsButton from '../../components/AddCarButton';
import CarsPanel from '../../components/CarsPanel';

class CarsDetails extends React.Component {

  constructor(props) {
    super(props);

    this.handleAddCarsClick = this.handleAddCarsClick.bind(this);
  }

  componentDidMount() {
    this.props.addCars();
  }

  handleAddCarsClick() {
    this.props.addCars();
  }

  renderCarsPanels() {
    return this.props.carTypes.map((carType) => 
      <CarsPanel 
        key={carType}
        panelType={carType}
        panelCars={this.props.carsData[carType]}
      />
    )
  }

  render() {
    
    return (
      <div>
        {this.props.errorMessage}
        <AddCarsButton handleAddCarClick={this.handleAddCarsClick} />
        {this.renderCarsPanels()}
      </div>
    );
  }
}

CarsDetails.propTypes = {
  carTypes: PropTypes.array.isRequired,
  carsData: PropTypes.object.isRequired,
  errorMessage: PropTypes.string,
};

const mapDispatchToProps = dispatch => {
  return {
    addCars: () => { requestAddCars(dispatch) }
  }
}

const mapStateToProps = state => {
  return {
    carTypes: state.carsDetailsReducer.carTypes,
    carsData: state.carsDetailsReducer.carsData,
    errorMessage: state.carsDetailsReducer.errorMessage
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CarsDetails);
