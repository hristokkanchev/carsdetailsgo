/**
 * CarsDetails Reducer
 */
import {
  CARS_DATA_FETCHING,
  CARS_DATA_FETCHED,
  CARS_DATA_FETCH_FAIL
} from './constants';


const initialState = {
  loading: true,
  errorMessage: '',
  carTypes: [],
  carsData: {},
};

/**
 * CarsDetails reducer.
 * @param {Object} state Redux State.
 * @param {Object} action Action
 */
const carsDetailsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CARS_DATA_FETCHING:
      return Object.assign({}, state, {
        loading: action.payload.loading
      });
      break;
    case CARS_DATA_FETCHED: {
      const { carsInfo } = action.payload;
      const newCarTypes = state.carTypes.slice();
      let newCarsData = Object.assign({}, state.carsData);
      
      carsInfo.results.forEach((car) => {
        // If the car make type is not present in the state.carType
        if (!newCarTypes.includes(car.type)) {
          // Add new car type. 
          newCarTypes.push(car.type);
          // Prepare state.carsData for adding new cars to the new car type.
          newCarsData[car.type] = [];
        }

        if (carsInfo.discount_percentage) {
          let discountedPrice = car.price * ((100 - carsInfo.discount_percentage) / 100);
          car.discountedPrice = parseFloat(discountedPrice.toFixed(2));
          car.discountPercentage = carsInfo.discount_percentage;
        }

        // Add the new cars to their respective car type.
        newCarsData[car.type].push(car);
      });
      
      return Object.assign({}, state, {
        loading: action.payload.loading,
        carTypes: newCarTypes,
        carsData: newCarsData
      });
      break;
    }
    case CARS_DATA_FETCH_FAIL: {
      const { loading, errorMessage } = action.payload;
      console.log(errorMessage);
      return Object.assign({}, state, {
        loading,
        errorMessage
      });
      break;
    }
    default:
      return state;
      break;
  }
}

export default carsDetailsReducer;