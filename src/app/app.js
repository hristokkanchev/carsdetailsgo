import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';

// Main application combined reducer.
import appReducers from './reducers';
// Main application container component.
import CarsDetails from './containers/CarsDetails';

const store = createStore(appReducers);

render(
  <Provider store={store}>
    <CarsDetails />
  </Provider>,
  document.querySelector('[data-component-name="CarsDetails"]')
);
