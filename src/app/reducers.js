import { combineReducers } from 'redux';

// Application reducers.
import carsDetailsReducer from './containers/CarsDetails/reducer';

export default combineReducers({
  carsDetailsReducer
});