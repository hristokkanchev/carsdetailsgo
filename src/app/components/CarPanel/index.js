import React from 'react';
import PropTypes from 'prop-types';

class CarPanel extends React.Component {
  render() {
    const {
      discountPercentage,
      make_model,
      type,
      provider,
      price,
      discountedPrice,
    } = this.props;

    return (
      <div className={`panel__item car${discountedPrice ? ' car--discounted': ''}` }>
        {(
          discountPercentage
          && <span className="car__discount-percentage">
            Save {discountPercentage}%
          </span>
        )}
        <div className="car__provider">
          <img src={`resources/images/${provider}-logo.jpg`} alt={provider} />
        </div>
        <div className="car__info">
          <h3 className="car__name">{make_model}</h3>
          <h4 className="car__type">{type}</h4>
          <p className="car__price car__price--standard">&pound; {price}</p>
          {(
            discountedPrice
            && <p className="car__price car__price--discount">&pound; {discountedPrice}</p>
          )}
        </div>
      </div>
    );
  }
}

CarPanel.propTypes = {
  make_model: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  provider: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  discountedPrice: PropTypes.number,
  discountPercentage: PropTypes.number,
};

export default CarPanel;
