import React from 'react';
import PropTypes from 'prop-types';

import CarPanel from '../CarPanel';

class CarsPanel extends React.Component {
  render() {
    return (
      <section className="panel">
        <div className="panel__title-wrap">
          <h2 className="panel__title">{this.props.panelType}</h2>
        </div>
        <div className="panel__content">
          {this.props.panelCars.map((car, carIndex) => 
            // We do not have anything unique about the cars 
            // so we are forced to have carIndex as key :(
            <CarPanel key={carIndex} {...car} />
          )}
        </div>
      </section>
    );
  }
}

CarsPanel.propTypes = {
  panelType: PropTypes.string.isRequired,
  panelCars: PropTypes.array.isRequired,
};

export default CarsPanel;
