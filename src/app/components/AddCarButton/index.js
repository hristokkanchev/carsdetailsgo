import React from 'react';
import PropTypes from 'prop-types';

class AddCarButton extends React.Component {

  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(e) {
    e.preventDefault();

    this.props.handleAddCarClick();
  }

  render() {
    return (
      <div className="cars-add">
        <button onClick={this.handleClick} type="button">Add Vehicle</button>
      </div>
    );
  }
}

AddCarButton.propTypes = {
  handleAddCarClick: PropTypes.func.isRequired,
};

export default AddCarButton;
