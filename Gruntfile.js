module.exports = function (grunt) {
  grunt.initConfig({
    'jasmine': {
      'src': 'src/**/*.js',
      'options': {
        'specs': 'test/compiled/**.spec.js'
      }
    },
    'copy': {
      'main': {
        'files': [{
          'expand': true,
          'cwd': 'src/js',
          'src': '**',
          'dest': 'dist/js/'
        }]
      }
    },
    'sass': {
      'dist': {
        'files': [{
          'expand': true,
          'cwd': 'src/scss',
          'src': ['**/*.scss'],
          'dest': './dist/css/',
          'ext': '.css'
        }]
      }
    },
    'autoprefixer': {
      'options': {
        'browsers': ['last 5 versions']
      },
      'dist':{
        'files':{
          './dist/css/main.css':'./dist/css/main.css'
        }
      }
    },
    'browserify': {
      'dist': {
        'files': {
          './dist/js/app.js': './src/app/app.js',
          './test/compiled/tests.spec.js': './test/spec/**.spec.js'
        },
        'options': {
          'transform': [['babelify']],
          'browserifyOptions': {
            'debug': true
          }
        }
      }
    },
    'watch': {
      'sass': {
        'files': ['src/scss/**/*.scss'],
        'tasks': ['sass']
      },
      'browserify': {
        'files': ['src/app/**/*.js', 'test/spec/**.spec.js'],
        'tasks': ['browserify']
      },
      'autoprefixer': {
        'files': ['src/scss/main.scss'],
        'tasks': ['autoprefixer']
      }
    },
    'browserSync': {
      'dev': {
        'bsFiles': {
          'src': [
            'dist/js/**/*.js',
            'dist/css/*.css',
            'dist/templates/*.html',
            'test/*.js'
          ]
        },
        'options': {
          'watchTask': true,
          'proxy': "http://localhost:3000",
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jasmine');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-autoprefixer');

  grunt.registerTask('default', ['browserSync', 'watch']);
};
