import 'babel-polyfill';
import {
  CARS_DATA_FETCH_FAIL,
  CARS_DATA_FETCHING,
  CARS_DATA_FETCHED,
} from '../../src/app/containers/CarsDetails/constants';
import {
  carsDataFetching,
  carsDataFetched,
  carsDataFetchFail,
} from '../../src/app/containers/CarsDetails/actions';
import carsDetailsReducer from '../../src/app/containers/CarsDetails/reducer';

(function () {
  'use strict';

  describe("CarDetails Actions", () => {
    it(`Fail action type should equal ${CARS_DATA_FETCH_FAIL}.`, () => {
      expect(carsDataFetchFail().type).toBe(CARS_DATA_FETCH_FAIL);
    });

    it(`Fetching action type should equal ${CARS_DATA_FETCHING}.`, () => {
      expect(carsDataFetching().type).toBe(CARS_DATA_FETCHING);
    });

    it(`Fetched action type should equal ${CARS_DATA_FETCHED}.`, () => {
      expect(carsDataFetched().type).toBe(CARS_DATA_FETCHED);
    });

    it(`Fetched action payload should contain array of car objects.`, () => {
      const mockCarsData = [{
        make_model: 'Audi',
        type: 'SUV',
        price: 22000
      }, {
        make_model: 'Jeep',
        type: 'SUV',
        price: 11000
      }];

      expect(carsDataFetched(mockCarsData).payload.carsInfo).toEqual(mockCarsData);
    });
  });

  describe("CarDetails Reducer", () => {
    it('Fetching action type handler - Redux state should have loading prop equal to true', () => {
      const newState = carsDetailsReducer(undefined, carsDataFetching());

      expect(newState.loading).toBe(true);
    });

    it('Fetched action type handler - Redux state should have carTypes and carsData values', () => {
      const mockAPIResult = JSON.parse('{"discount_percentage":15,"results":[{"make_model":"Audi Q5","type":"SUV","provider":"avis","price":33.17},{"make_model":"Ford Fiesta","type":"Compact","provider":"budget","price":42.44},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":57.97},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":50.61},{"make_model":"Ford Ka","type":"Compact","provider":"budget","price":57.38}]}');
      const newState = carsDetailsReducer(undefined, carsDataFetched(mockAPIResult));

      expect(newState.carTypes.length).not.toEqual(0);
      expect(Object.keys(newState.carsData)).not.toEqual(0);
    });

    it('Fetched action type handler - Redux state should contain cars with discountedPrice and discountPercentage if discount_percentage is present.', () => {
      const mockAPIResult = JSON.parse('{"discount_percentage":15,"results":[{"make_model":"Audi Q5","type":"SUV","provider":"avis","price":33.17},{"make_model":"Ford Fiesta","type":"Compact","provider":"budget","price":42.44},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":57.97},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":50.61},{"make_model":"Ford Ka","type":"Compact","provider":"budget","price":57.38}]}');
      const newState = carsDetailsReducer(undefined, carsDataFetched(mockAPIResult));

      Object.keys(newState.carsData).forEach((carType) => {
        newState.carsData[carType].forEach((car) => {
          expect(car.hasOwnProperty('discountPercentage')).toBe(true);
          expect(car.hasOwnProperty('discountedPrice')).toBe(true);
        });
      });
    });

    it('Fetched action type handler - Redux state should NOT contain cars with discountedPrice and discountPercentage if there is no discount_percentage.', () => {
      const mockAPIResult = JSON.parse('{"results":[{"make_model":"Audi Q5","type":"SUV","provider":"avis","price":33.17},{"make_model":"Ford Fiesta","type":"Compact","provider":"budget","price":42.44},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":57.97},{"make_model":"Vauxhall Insignia","type":"Estate","provider":"budget","price":50.61},{"make_model":"Ford Ka","type":"Compact","provider":"budget","price":57.38}]}');
      const newState = carsDetailsReducer(undefined, carsDataFetched(mockAPIResult));

      Object.keys(newState.carsData).forEach((carType) => {
        newState.carsData[carType].forEach((car) => {
          expect(car.hasOwnProperty('discountPercentage')).toBe(false);
          expect(car.hasOwnProperty('discountedPrice')).toBe(false);
        });
      });
    });

    it('Fetch fail action type handler - Redux state should contain an error message', () => {
      const errorMessage = 'Error message';
      const newState = carsDetailsReducer(undefined, carsDataFetchFail(errorMessage));

      expect(newState.errorMessage).toBe(errorMessage);
    });
  });
})();


